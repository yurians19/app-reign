import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";

import { Article } from "../models/article";

@Injectable({
  providedIn: 'root'
})
export class ArticleService {
  readonly URL_API = "http://api.amagpieinthesky.com";
  Articles: Article[];

  constructor(private http: HttpClient) { }

  getArticles() {
    return this.http.get<Article[]>(`${this.URL_API}/article`);
  }

  deleteArticle(_id: string) {
    return this.http.delete(`${this.URL_API}/article/${_id}`);
  }
}
